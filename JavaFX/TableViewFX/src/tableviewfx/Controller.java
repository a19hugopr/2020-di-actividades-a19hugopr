/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableviewfx;



import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author hugo
 */
public class Controller implements Initializable{
    
  
    @FXML
    private TableView<Person> table;
    @FXML
    private TableColumn<Person, String> name;
    @FXML
    private TableColumn<Person, String> surname;
    @FXML
    private TableColumn<Person, String> email;
    @FXML
    private TextField tfname;   
    @FXML
    private TextField tfsurname;    
    @FXML
    private TextField tfemail;
    @FXML        
    private Button btnadd;
    @FXML        
    private Button btnmodificar;
    @FXML 
    private Button btnborrar;
    @FXML
    private void agregarPersona(ActionEvent event){
        String nombre = tfname.getText();
        String apellido = tfsurname.getText();
        String email = tfemail.getText();
        
        Person p = new Person(nombre,apellido,email);
        
        list.add(p);
        table.setItems(list);
    }
    
  
    ObservableList<Person> list = FXCollections.observableArrayList(
    
            new Person("Hugo", "Pires", "a@gmail.com"),
            new Person("Juan", "Rodríguez","b@gmail.com")
    );
    @FXML
    private void seleccionar(MouseEvent event){
        
        Person p = table.getSelectionModel().getSelectedItem();
    }
    @FXML
    private void modificar(ActionEvent event){
        
        Person p = table.getSelectionModel().getSelectedItem();
        
        String nombre = tfname.getText();
        String apellido = tfsurname.getText();
        String email = tfemail.getText();
        
        Person aux = new Person(nombre,apellido,email);
        
        p.setName(aux.getName());
        p.setSurname(aux.getSurname());
        p.setEmail(aux.getEmail());
        
        table.refresh();
        
    }
    
     @FXML
    private void eliminar(ActionEvent event) {

        Person p = table.getSelectionModel().getSelectedItem();

            list.remove(p);
            table.refresh();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle("Info");
            alert.setContentText("Persona eliminada");
            alert.showAndWait();

        }
    
   
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
        name.setCellValueFactory(new PropertyValueFactory<Person,String>("name"));
        surname.setCellValueFactory(new PropertyValueFactory<Person,String>("surname"));
        email.setCellValueFactory(new PropertyValueFactory<Person,String>("email"));
        
        table.setItems(list);
    
}
    
}
