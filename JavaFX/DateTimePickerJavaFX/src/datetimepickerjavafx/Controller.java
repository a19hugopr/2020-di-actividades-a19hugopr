/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datetimepickerjavafx;


import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;

/**
 *
 * @author hugo
 */
public class Controller {

 @FXML
 private DatePicker myDatePicker;
 @FXML
 private Label myLabel;
 @FXML
 public void getDate(ActionEvent event) {

  LocalDate myDate = myDatePicker.getValue();

  String myFormattedDate = myDate.format(DateTimeFormatter.ofPattern("dd-MMM-yy"));

  myLabel.setText(myFormattedDate);

 }

    public void initialize(URL url, ResourceBundle rb) {
        
    }
}
