/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loginfx;


import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class Controller implements Initializable{
 
    @FXML
    private Label labeltitulo;
    @FXML
    private Label labelusername;
    @FXML
    private Label labelpassword;
    @FXML
    private Label labelalerta;
    @FXML
    private TextField textusername;
    @FXML
    private TextField textpassword;
    @FXML
    private Button login;
    
    
    @FXML
    public void getData(ActionEvent event) {
    if(textusername.getText().isEmpty() && textpassword.getText().isEmpty()){
    labelalerta.setVisible(false);
    }
    else{
    labelalerta.setText("Botón pulsado");
    labelalerta.setTextFill(Color.RED);
    labelalerta.setVisible(true);
    
    }
               
    
    }

    
     public void initialize(URL url, ResourceBundle rb) {
        labelalerta.setVisible(false);
    }
}
