/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progress;


import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author a19hugopr
 */
public class Controller implements Initializable{
    @FXML
    private Button aumentar;
    @FXML
    private Button decrecer;
    @FXML
    private ProgressBar progreso;
    @FXML
    private ProgressIndicator indicator;
    
    final Numero myNum = new Numero();

    @FXML
    private void buttonaumentar(ActionEvent event)   {
     myNum.setNumber(myNum.getNumber()+0.1);
     
    }
    @FXML
      private void buttodisminuir()   {
    myNum.setNumber(myNum.getNumber()-0.1);
  
    }
      
    public void initialize(URL location, ResourceBundle resources) {
              
       myNum.setNumber(0);
       myNum.numberProperty().addListener(new ChangeListener<Object>() {
       @Override
       public void changed(ObservableValue<? extends Object> ov, Object t, Object t1) {
       progreso.progressProperty().bind(myNum.numberProperty());
       indicator.progressProperty().bind(myNum.numberProperty());
       }
   });
    
        
 


      
   
    }
      
}