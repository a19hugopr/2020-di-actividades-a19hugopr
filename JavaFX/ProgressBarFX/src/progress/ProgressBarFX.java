/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progress;


import java.io.File;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author a19hugopr
 */
public class ProgressBarFX extends Application{
      
    public void start(Stage stage) throws Exception {
      
        
        Parent root = FXMLLoader.load(getClass().getResource("progressfx.fxml"));
       

        Scene scene = new Scene(root);   
       
        stage.setScene(scene);
        stage.setTitle("ProgressBarFX");
        stage.show();
     
     
       
    }
    
    
    
    
    public static void main(String[] args) {       
        launch(args);    }

 

   

   
  
    }


