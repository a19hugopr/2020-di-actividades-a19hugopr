/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacionGestion.TableModel;

import java.awt.Component;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import aplicacionGestion.gui.dto.Cliente;

/**
 *
 * @author hugo
 */
public class ClienteTableModel extends GenericDomainTableModel<Cliente> {
    
    private List<Cliente> listaClientes; 
    private String[] columnas={"Nome","Apelidos","Data de alta","Provincia"};

    public ClienteTableModel(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }
    
    @Override
    public int getRowCount() {
        return listaClientes.size();
    }

    @Override
    public int getColumnCount() {
     return columnas.length; 
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return listaClientes.get(rowIndex).getNome();
            case 1: 
                return listaClientes.get(rowIndex).getApelidos();
            case 2: 
                return listaClientes.get(rowIndex).getDataAlta();
            case 3: 
                return listaClientes.get(rowIndex).getProvincia();
        }
        return null;
    }

   @Override
    public String getColumnName(int column) {
       if (column < 0 || column > getColumnCount())
        throw new ArrayIndexOutOfBoundsException(column);
        else
       return columnas[column]; //To change body of generated methods, choose Tools | Templates.
    }

     @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0: return String.class;
            case 1: return String.class;
            case 2: return String.class;
            case 3: return String.class;
        }
        throw new ArrayIndexOutOfBoundsException(columnIndex);
    }


    @Override
   public void setValueAt(Object aValue, int rowIndex, int columnIndex)
   {
       Cliente row = listaClientes.get(rowIndex);
       if(0 == columnIndex) {
           row.setNome((String) aValue);
       }
       else if(1 == columnIndex) {
           row.setApelidos((String) aValue);
       }
       else if(2 == columnIndex) {
           row.setDataAlta((Date) aValue);
       }
       else if(3 == columnIndex) {
           row.setProvincia((String) aValue);
       }
   }
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    } //To change body of generatedmethods, choose Tools | Templates.}










 
   


    
    

  
  
    
    
}
