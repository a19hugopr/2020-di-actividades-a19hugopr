/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacionGestion.TableModel;
import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import aplicacionGestion.PantallaPrincipal;
/**
 *
 * @author hugo
 */
public class DateCellEditor extends AbstractCellEditor implements TableCellEditor {

  
    private JSpinner spinner;

    public DateCellEditor(String dateFormatPattern) {
        
       

        SpinnerDateModel dateModel = new SpinnerDateModel(new Date(), null, null, Calendar.DAY_OF_MONTH);
        spinner = new JSpinner(dateModel);
        spinner.setEditor(new JSpinner.DateEditor(spinner, dateFormatPattern));
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        spinner.setValue(value);
        return spinner;
    }

    @Override
    public Object getCellEditorValue() {
        return spinner.getValue();
    }
    
}
