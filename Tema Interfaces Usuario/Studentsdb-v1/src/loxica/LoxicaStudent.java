/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loxica;

import interfaz.PantallaPrincipal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import student.Student;

/**
 *
 * @author a19hugopr
 */
public class LoxicaStudent {
  Connection conexion=null;
    Student student = new Student();


    
  
    public LoxicaStudent() {
           
    }

    private void abrirConexion() {
            try {         
             Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
             String url = "jdbc:derby:baseDatos;create=true";
             conexion = DriverManager.getConnection(url, "usuario", "abc123.");
               
              
        }
        catch (Exception erro)  {          
            System.out.println("Erro Abrir conexión: " + erro.getMessage());
        }  
      }

    public Connection getConexion() {
        return conexion;
    }
    
 

    public void cerrarConexion() {
         try {
        conexion.close();
        }
        catch (Exception erro) {      
    System.out.println("Erro: " + erro.getMessage());     
}
    }

    public ArrayList<Student> getStudentsList() {
        abrirConexion();
        
          String sql = "SELECT * FROM APP.STUDENT";
          ArrayList<Student> arrStudent = new ArrayList<Student>();
     
    
          try{
              Statement st;
              st = conexion.createStatement();
              ResultSet rs = st.executeQuery(sql);
              
            
               while(rs.next()){
              arrStudent.add(new Student(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4)));
             
     
           }
               cerrarConexion();
            
            
        }
        
        catch(SQLException e){
            e.printStackTrace();
        }
        return arrStudent;
        
    }




  public int altaStudent(Student student) throws Exception {
        abrirConexion();
        PreparedStatement ps;
 
 try{
     ps=conexion.prepareStatement("INSERT INTO APP.STUDENT(DNI,NAME,SURNAME,AGE) values (?,?,?,?)");
     ps.setString(1,student.getDNI());
     ps.setString(2,student.getNAME());
     ps.setString(3,student.getSURNAME());
     ps.setString(4,student.getAGE()); 
     
     ps.executeUpdate();
 }
 catch(SQLException ex){
     ex.printStackTrace();
 }
 return 1;
   }
  
   public int eliminarStudent(String dni) throws Exception {
       
       abrirConexion();
 
        String cadeaBorrarSql = "delete from APP.STUDENT where DNI=?";
        PreparedStatement ps = conexion.prepareStatement(cadeaBorrarSql);
        ps.setString(1, dni);
        int resultado = ps.executeUpdate();
        ps.close();
        return resultado;
        
        
      
    }
   
      public int actualizarStudent(Student student) throws Exception {
        abrirConexion();
        String cadeaModificarSql = "update APP.STUDENT set NAME =?," + "SURNAME = ?,AGE=? where DNI =?";
        PreparedStatement ps = conexion.prepareStatement(cadeaModificarSql);
        ps.setString(1, student.getNAME());
        ps.setString(2, student.getSURNAME());
        ps.setString(3, student.getAGE());
        ps.setString(4, student.getDNI());
        int resultado = ps.executeUpdate();
        ps.close();
        return resultado;

    }
   
    

}
