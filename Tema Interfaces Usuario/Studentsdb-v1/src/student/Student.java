/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student;

import java.util.ArrayList;

/**
 *
 * @author a19hugopr
 */
public class Student {
    
    private String DNI;
    private String NAME;
    private String SURNAME;
    private String AGE;

    public Student(String DNI, String NAME, String SURNAME, String AGE) {
        this.DNI = DNI;
        this.NAME = NAME;
        this.SURNAME = SURNAME;
        this.AGE = AGE;
    }

    public Student() {
    }
    

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getSURNAME() {
        return SURNAME;
    }

    public void setSURNAME(String SURNAME) {
        this.SURNAME = SURNAME;
    }

    public String getAGE() {
        return AGE;
    }

    public void setAGE(String AGE) {
        this.AGE = AGE;
    }
    
     public ArrayList toArrayList(){
        ArrayList<Student> arrStudent = new ArrayList<Student>(4);
        
        arrStudent.add(new Student(DNI,NAME,SURNAME,AGE));

        
        return arrStudent;
    }
      public String[] toArray(){
        String[] array = new String[4];
        
        array[0] = DNI;
        array[1] = NAME;
        array[2] = SURNAME;
        array[3] = AGE;
        
        return array;
    }
    
}
