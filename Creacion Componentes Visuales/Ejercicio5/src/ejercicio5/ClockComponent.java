/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio5;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JLabel;

/**
 *
 * @author hugo
 */
public class ClockComponent extends JLabel implements Serializable{
   
    boolean format12 = false;

    public ClockComponent() {
    }

    public boolean format12() {
        return format12;
    }

    public void setFormat12(boolean format12) {
        this.format12 = format12;
    }
    
    public void clock(){
        
        Thread clock = new Thread(){
            
        public void run(){
            
        try{ 
            
        for(;;){
            
         Calendar cal = new GregorianCalendar();        
         int hour = cal.get(Calendar.HOUR);
         int hourdy = cal.get(Calendar.HOUR_OF_DAY);
         int minute = cal.get(Calendar.MINUTE);   
         setText((format12? hour : hourdy) + ":" + minute);
         sleep(1000);
        
        }
        }
        catch(InterruptedException e){
        e.printStackTrace();
        }
      
        }
        };
        
        clock.start();
        
            }
                
}
    

    

