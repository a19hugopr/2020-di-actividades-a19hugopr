/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

import java.awt.Color;
import java.io.Serializable;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author hugo
 */
public class JPanelColor extends JButton implements Serializable{
    
    private Color colorfondo;
    private Color colortexto;
    
    public JPanelColor() {
    }

    public Color getColorfondo() {
        return colorfondo;
    }

    public void setColorfondo(Color colorfondo) {
        this.colorfondo = colorfondo;
        setBackground(colorfondo);
    }

    public Color getColortexto() {
        return colortexto;
    }

    public void setColortexto(Color colortexto) {
        this.colortexto = colortexto;
        setForeground(colortexto);
     
    }
    
    
    
}
