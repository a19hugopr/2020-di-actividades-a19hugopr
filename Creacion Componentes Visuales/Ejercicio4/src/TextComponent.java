
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.Serializable;
import javax.swing.JLabel;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hugo
 */
public class TextComponent extends JTextField implements Serializable{
    
    private boolean is_numeric;
 

        
    public TextComponent(){
        is_numeric = false;
        setText("");
        
        this.addKeyListener(new KeyListener(){
            @Override
            public void keyTyped(KeyEvent e) {
       
            }

            @Override
            public void keyPressed(KeyEvent e) {
            char c = e.getKeyChar();
            if(Character.isLetter(c)){
                devuelve(false);
                setEditable(false);  
            }
            else{
                devuelve(true);
                setEditable(true);
            }
            }

            @Override
            public void keyReleased(KeyEvent e) {
        
            }   
        });
    }   
     public void devuelve(boolean is_numeric){
            if(is_numeric==true){
            System.out.println("Has introducido un número");
            }
            else{
            System.out.println("No puedes introducir letras");   
            }
    
}
}
