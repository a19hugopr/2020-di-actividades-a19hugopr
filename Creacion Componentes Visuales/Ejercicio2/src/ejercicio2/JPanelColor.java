/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import java.io.Serializable;
import javax.swing.JButton;

/**
 *
 * @author hugo
 */

    public class JPanelColor extends JButton implements Serializable{
    
    private ColorTipo colortipo;

    public JPanelColor() {
    }

    public ColorTipo getColortipo() {
        return colortipo;
    }

    public void setColortipo(ColorTipo colortipo) {
        this.colortipo = colortipo;
        setForeground(colortipo.getColortexto());
        setBackground(colortipo.getColorfondo());
    }

   
    
}
