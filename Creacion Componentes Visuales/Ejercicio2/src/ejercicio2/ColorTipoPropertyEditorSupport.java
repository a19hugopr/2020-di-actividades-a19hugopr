/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import java.awt.Component;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author hugo
 */
public class ColorTipoPropertyEditorSupport extends PropertyEditorSupport{
    
private InterfazComponent jpanelcolor = new InterfazComponent();
@Override
public boolean supportsCustomEditor() {
return true;
}
@Override
public Component getCustomEditor() {
return jpanelcolor;
}
@Override
public String getJavaInitializationString() {
ColorTipo colortipo=jpanelcolor.getSelectedValue();      
return "new ejercicio2.ColorTipo("+"new java.awt.Color("+jpanelcolor.getSelectedValue().getColortexto().getRGB()+"),new java.awt.Color("+jpanelcolor.getSelectedValue().getColorfondo().getRGB()+"))";
}
@Override
public Object getValue() {
return jpanelcolor.getSelectedValue();
}
}
