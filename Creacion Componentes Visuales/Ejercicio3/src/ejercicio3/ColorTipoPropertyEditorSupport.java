/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

import java.awt.Component;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author hugo
 */
public class ColorTipoPropertyEditorSupport extends PropertyEditorSupport{
    
    private InterfazComponent jpanelcolortipo = new InterfazComponent();
@Override
public boolean supportsCustomEditor() {
return true;
}
@Override
public Component getCustomEditor() {
return jpanelcolortipo;
}
@Override
public String getJavaInitializationString() {
ColorTipo colortipo=jpanelcolortipo.getSelectedValue();      
return "new ejercicio3.ColorTipo("+"new java.awt.Color("+jpanelcolortipo.getSelectedValue().getColortexto().getRGB()+"),new java.awt.Color("+jpanelcolortipo.getSelectedValue().getColorfondo().getRGB()+"))";
}
@Override
public Object getValue() {
return jpanelcolortipo.getSelectedValue();
}
}
