/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

import java.awt.Color;
import java.io.File;
import java.io.Serializable;

/**
 *
 * @author hugo
 */
public class ColorTipo implements Serializable{

private Color colorfondo;
private Color colortexto;

    public ColorTipo(Color colorfondo, Color colortexto) {
        this.colorfondo = colorfondo;
        this.colortexto = colortexto;
    }

    public Color getColorfondo() {
        return colorfondo;
    }

    public void setColorfondo(Color colorfondo) {
        this.colorfondo = colorfondo;
    }

    public Color getColortexto() {
        return colortexto;
    }

    public void setColortexto(Color colortexto) {
        this.colortexto = colortexto;
    }

   




}
