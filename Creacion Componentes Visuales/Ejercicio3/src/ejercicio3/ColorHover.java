/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.io.Serializable;

/**
 *
 * @author a19hugopr
 */
public class ColorHover implements Serializable {
    
    private Color colortextohover;
    private Color colorfondohover;


    public ColorHover(Color colortextohover, Color colorfondohover) {
        this.colortextohover = colortextohover;
        this.colorfondohover = colorfondohover;
    }

    public Color getColortextohover() {
        return colortextohover;
    }

    public void setColortextohover(Color colortextohover) {
        this.colortextohover = colortextohover;
        
    }

    public Color getColorfondohover() {
        return colorfondohover;
    }

    public void setColorfondohover(Color colorfondohover) {
        this.colorfondohover = colorfondohover;
    }
    

    
}
