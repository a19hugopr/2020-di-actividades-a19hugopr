/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import javax.swing.JButton;
import javax.swing.UIManager;

/**
 *
 * @author hugo
 */

    public class JPanelColor extends JButton implements Serializable{
    
    private ColorTipo colortipo;
    private ColorHover colorhover;
    private MouseEvent evt;


    public JPanelColor() {
        
         this.addMouseListener(new MouseAdapter(){
         
         @Override
         public void mouseEntered(MouseEvent e){
    
        setForeground(colorhover.getColortextohover());
        setBackground(colorhover.getColorfondohover());
         }
         @Override
         public void mouseExited(MouseEvent e){

             if(colortipo!=null){
                setColortipo(colortipo);   
             }
             else{
                 setBackground(null);
                 setForeground(null);
             }
           
         }
         
         });
    }


    public ColorHover getColorhover() {
        return colorhover;
    }

    public void setColorhover(ColorHover colorhover) { 
        this.colorhover = colorhover;  
        }

    public ColorTipo getColortipo() {
        return colortipo;
    }

    public void setColortipo(ColorTipo colortipo) {
        this.colortipo = colortipo;
         setBackground(colortipo.getColorfondo());
         setForeground(colortipo.getColortexto());
        
    }
    
  
        }
    
        
        
   
    


     
    
     
    
 

    

   
    

