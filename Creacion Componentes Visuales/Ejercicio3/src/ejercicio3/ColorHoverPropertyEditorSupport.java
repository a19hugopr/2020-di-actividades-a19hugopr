/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

import java.awt.Component;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author a19hugopr
 */
public class ColorHoverPropertyEditorSupport extends PropertyEditorSupport{
    
private InterfazComponentHover jpanelcolorhover = new InterfazComponentHover();
@Override
public boolean supportsCustomEditor() {
return true;
}
@Override
public Component getCustomEditor() {
return jpanelcolorhover;
}
@Override
public String getJavaInitializationString() {
ColorHover colortipo=jpanelcolorhover.getSelectedValue();      
return "new ejercicio3.ColorHover("+"new java.awt.Color("+jpanelcolorhover.getSelectedValue().getColortextohover().getRGB()+"),new java.awt.Color("+jpanelcolorhover.getSelectedValue().getColorfondohover().getRGB()+"))";
}
@Override
public Object getValue() {
return jpanelcolorhover.getSelectedValue();
}

}
