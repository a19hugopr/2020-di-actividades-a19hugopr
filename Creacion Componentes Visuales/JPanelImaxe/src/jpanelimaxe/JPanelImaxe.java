/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpanelimaxe;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.File;
import java.io.Serializable;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author a19hugopr
 */
public class JPanelImaxe extends JPanel implements Serializable{

   private ImagenFondo imaxeFondo;
    
    
    public ImagenFondo getImaxeFondo() {
    return imaxeFondo;
}

   public void setImaxeFondo(ImagenFondo imaxeFondo) {
this.imaxeFondo = imaxeFondo;
// Hai que volver a pintar o compoñente cada vez que se cambia a imaxe
repaint();
}
@Override
protected void paintComponent(Graphics g) {
super.paintComponent(g);
// Para evitar un NullPointerException antes de iniciar a propiedade
if (imaxeFondo != null) {
if (imaxeFondo.getRutaImaxe() != null && imaxeFondo.getRutaImaxe().exists()) {
ImageIcon imageIcon = new ImageIcon(imaxeFondo.getRutaImaxe().getAbsolutePath());
Graphics2D g2d = (Graphics2D) g;
g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
imaxeFondo.getOpacidade()));
g.drawImage(imageIcon.getImage(), 0, 0, null);
// Unha vez cambiada a opacidade, hai que volver a poñela en 1
g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1));
}
}


}
    
  
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
