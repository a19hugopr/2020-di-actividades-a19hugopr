/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpanelimaxe;

import java.awt.Component;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author a19hugopr
 */
public class ImagenFondoPropertyEditorSupport extends PropertyEditorSupport {
    
private ImaxeFondoPanel imaxeFondoPanel = new ImaxeFondoPanel();
@Override
public boolean supportsCustomEditor() {
return true;
}
@Override
public Component getCustomEditor() {
return imaxeFondoPanel;
}
@Override
public String getJavaInitializationString() {
ImagenFondo imaxeFondo = imaxeFondoPanel.getSelectedValue();
return "new jpanelimaxe.ImagenFondo("+"new java.io.File(\""+
imaxeFondo.getRutaImaxe().getAbsolutePath()+"\"), "+ imaxeFondo.getOpacidade()+"f)";
}
@Override
public Object getValue() {
return imaxeFondoPanel.getSelectedValue();
}
}
