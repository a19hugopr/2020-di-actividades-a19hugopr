/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpanelimaxe;

import java.io.File;
import java.io.Serializable;

/**
 *
 * @author a19hugopr
 */
public class ImagenFondo implements Serializable {
    
private File rutaImaxe;
private Float opacidade;

public ImagenFondo(File rutaImaxe, Float opacidade) {
this.rutaImaxe = rutaImaxe;
this.opacidade = opacidade;
}

public File getRutaImaxe() {
return rutaImaxe;
}

public void setRutaImaxe(File rutaImaxe) {
this.rutaImaxe = rutaImaxe;
}
public Float getOpacidade() {
return opacidade;
}
public void setOpacidade(Float opacidade) {
this.opacidade = opacidade;
}

}
